require "TSLib"
require "core"

w,h = getScreenSize()
UINew(1,"基本设置","开始","取消","uiconfig.dat",0,120,w*0.8,h*0.8,"221,240,237","88,210,232")
UILabel({text="初始化："})
UICombo("initNum","竖屏,右边,左边")
UILabel({text="刷图等待时间(分钟)："})
UIEdit("waitTime","请填写数字","4",15,"left","255,0,0")
UIShow()

if initNum == "竖屏" then
	initNum = 0
elseif initNum == "右边" then
	initNum = 1
else
	initNum = 2
end

init(initNum)

local home_setting = {{1802,42,0x2e1f19},{1782,38,0xcbb491},{1798,24,0xcebe9e},{1822,40,0xcbb491},{1804,56,0xc4ab84},}
local confirm_button = {1740,824}
local sanguokill = "com.gale.sanguokill.hd"

function startWuhuan()
	while true do 
		x, y = findPic("单人",{349,298,753,486})
		if x ~= -1 then
			click(x, y)
			mSleep(1000)
			x, y = findPic("武皇",{952,538,1205,721})
			if x ~= -1 then
				click(x, y)
				--等待确认界面
				checkRetryColor({{692,938,0x194a54},{692,954,0x083745},})
				--点击确认按钮
				click(778, 942)
				--进去选择武将界面
				clickCheckRetryColor(x, y, {{545,29,0xfbe07f},{545,36,0xecbf57},})
				--选择武将
				while true do
					changeWuJian()
				end
			end
		else
			--抛出异常进行异常检查
			error("未在游戏主页")
		end
	end
end

--选择武将
function changeWuJian()
	--等待到有菜单设置界面
	checkRetryPic("菜单设置",{445,0,688,70})
	mSleep(2000)
	step("选择左幽")
	click(1717,450)
	mSleep(1000)
	step("选择其他武将")
	local wuJian = {"394,435","634,429","840,441","1057,442","1293,447","413,636","607,643","862,648","1094,653","1282,653"}
	for i, v in pairs(wuJian) do  
		local xy = strSplit(v, ",")
		click(xy[1], xy[2])
		mSleep(1000)
		--如果已经可以点击确定
		if (multiColor({{1648,806,0x1a4b56},{1654,836,0x083745},})) then
			click(confirm_button)
			break
		end
	end
	mSleep(1000)
	--组成双将
	click(confirm_button)
	--等待到有菜单设置界面
	checkRetryPic("菜单设置",{445,0,688,70})
	step("开启托管")
	trusteeship()
	step("等待结束")
	waitEnd()
	step("选择功勋")
	changeGonxun()
end

--退出游戏
function gameOut()
	--点击菜单设置
	click(568,37)
	--菜单界面
	checkRetryColor({{282,533,0x194a55},{840,538,0x134d35},{1401,534,0x62281c},})
	click(1516,538)
	--战功统计界面
	checkRetryColor({{688,917,0x1a4c56},{1078,922,0x561f11},})
	--点击退出
	click(1149,921)
	--等待到首页
	checkRetryColor(home_setting)
end
--开启托管
function trusteeship()
	for i = 5, 1, -1 do
		x, y = findPic("开启托管",{1214,2,1496,82})
		if x == -1 then
			click(1332, 44)
			mSleep(1000)
		else
			break
		end
	end
end

--等待结束 结束有两种 失败重玩界面 胜利到继续、确认-点击到功勋选择界面
function waitEnd()
	--重玩按钮 10分钟
	for i = tonumber(waitTime)*15, 1, -1 do
		--已经到重玩界面 可以点击
		x,y = findPic("重玩_按钮",{646,873,890,966})
		if x ~= -1 then
			return
		end
		mSleep(2000)
		--已经到重玩界面 不可以点击
		x,y = findPic("重玩_灰_按钮",{646,873,890,966})
		if x ~= -1 then
			return
		end
		--完结界面点击继续
		if multiColor({{485,25,0xfef095},{497,26,0xfae88f},{490,44,0xcfa145},}) == false then
			click(960,933)
			mSleep(1000)
		end
		mSleep(2000)
	end
	--超时退出
	step("超时退出")
	--点击菜单设置
	click(568,37)
	--菜单界面
	checkRetryColor({{282,533,0x194a55},{840,538,0x134d35},{1401,534,0x62281c},})
	click(1516,538)
	mSleep(2000)
end

--选择功勋
function changeGonxun()
	for i = 10, 1, -1 do
		x, y = findPic("重玩_按钮",{641,864,900,977})
		if x ~= -1 then
			click(x, y)
			break
		end
		--点击中间位置
		click(966,710)
		mSleep(1000)
		click(866,716)
		mSleep(1000)
	end
end

function checkError()
	--异常重开游戏
	gameAgin()
end

function startGame()
	closeApp(sanguokill)
	mSleep(1000)
	runApp(sanguokill)
	mSleep(5000)
	for i = 10, 1, -1 do
		if multiColor({{1762,99,0xffffff},{1770,106,0xffffff},{1777,117,0xffffff},{1778,100,0xffffff},{1761,115,0xffffff},}) then
			click(1768,106)
		end
		x, y = findPic("单人",{349,298,753,486})
		if x ~= -1 then
			break
		end
		mSleep(2000)
	end
end

function gameAgin()
	startGame()
	--确认部将系统
	click(1122,990)
	mSleep(2000)
	for i = 20, 1, -1 do
		if multiColor({{1762,99,0xffffff},{1770,106,0xffffff},{1777,117,0xffffff},{1778,100,0xffffff},{1761,115,0xffffff},}) then
			click(1768,106)
		end
		x, y = findPic("左幽_断吼",{170,614,836,764})
		if x ~= -1 then
			click(1825,985)
			mSleep(2000)
			return
		end
		mSleep(2000)
	end
	--未加载出左幽
	gameAgin()
end

while true do
	mSleep(2000)
	local isfront = isFrontApp(sanguokill)
	if (isfront ~= 1) then
		gameAgin()
	end
	pcall(function ()
			startWuhuan()
		end)
	step("检查异常")
	checkError()
	mSleep(5000)
end

