JsonUtils = require "JsonUtils"

function logger(...)
	local msg = ''
	for k,v in pairs({...}) do
		local str
		if type(v) == "table" then
			str = JsonUtils.encode(v)
		else
			str = tostring(v)
		end
		msg = string.format('%s %s ', msg, str)
	end
	nLog(msg)
	toast(msg)
end

function click(x, y, info)
	if type(x) == "table" then
		info = y
		y = x[2]
		x = x[1]
	end

	if(info ~= nil) then
		logger("点击["..info.."]",x,y)
	end

	touchDown(1,x, y)
	mSleep(math.random(400,600))                --某些特殊情况需要增大延迟才能模拟点击效果
	touchUp(1, x, y)
end
--步骤
function step(...)
	local msg = ''
	for k,v in pairs({...}) do
		local str
		if type(v) == "table" then
			str = JsonUtils.encode(v)
		else
			str = tostring(v)
		end
		msg = string.format('%s %s ', msg, str)
	end
	msg = string.format('进度:[%s]', msg)
	nLog(msg)
end
-- 找图片
function findPic(pic, range, degree, mask)
	if mask == nil then mask = 0 end
	if degree == nil then degree = 90 end
	pic = pic .. ".png"
	
	return findImageInRegionFuzzy(pic, degree, range[1],range[2],range[3],range[4], mask);
end

-- 模拟滑动操作，从点(x1, y1)划到到(x2, y2)。在妙战天下中失效，使用moveAngle
--function move(x1,y1,x2,y2)
--	local step, x, y, index = 20, x1 , y1, math.random(1,5)
--	touchDown(index, x, y)

--	local function move(from, to) 
--		if from > to then
--			do 
--				return -1 * step 
--			end
--		else 
--			return step 
--		end 
--	end

--	while (math.abs(x-x2) >= step) or (math.abs(y-y2) >= step) do
--		if math.abs(x-x2) >= step then x = x + move(x1,x2) end
--		if math.abs(y-y2) >= step then y = y + move(y1,y2) end
--		touchMove(index, x, y)
--		mSleep(math.random(400,600))
--	end

--	touchMove(index, x2, y2)
--	mSleep(math.random(400,600))
--	touchUp(index, x2, y2)
--end

--角度滑动
function moveAngle(x,y,angle,length,step)
	if (step == nil) then
		moveTowards(x,y,angle,length)
	else 
		moveTowards(x,y,angle,length,step)
	end
end

--重试比色（颜色、次数、间隔、判断）
function checkRetryColor(color,num,space,flag)
	local useTime = 0
	if num == nil then
		num = 20
	end
	if space == nil then
		space = 1000
	end
	if flag == nil then
		flag = true
	end
	for i=num,1,-1 do
		if flag == multiColor(color) then
			return true
		end
		mSleep(space)
		useTime = useTime + space
	end
	local errorInfo = "等待颜色耗时:"..tostring(useTime/1000).."秒。".."重试超时："..JsonUtils.encode(color)
	logger(errorInfo)
	error(errorInfo)
end

--重试比色（图片、次数、间隔、判断）
function checkRetryPic(pic,range,num,space,flag)
	local useTime = 0
	if num == nil then
		num = 20
	end
	if space == nil then
		space = 1000
	end
	if flag == nil then
		flag = true
	end
	for i=num,1,-1 do
		x, y = findPic(pic, range)
		if flag then
			if x ~= -1 then
				return true
			end
		else
			if x == -1 then
				return true
			end
		end
		mSleep(space)
		useTime = useTime + space
	end
	local errorInfo = "等待图片耗时:"..tostring(useTime/1000).."秒。".."重试超时："..pic
	logger(errorInfo)
	error(errorInfo)
end

--点击重试
function clickCheckRetryColor(x,y,color,num,space)
	if type(x) == "table" then
		space = num
		num = color
		color = y
		y = x[2]
		x = x[1]
	end

	local useTime = 0
	if num == nil then
		num = 40
	end
	if space == nil then
		space = 500
	end
	for i=num,1,-1 do
		click(x,y)
		if multiColor(color) then
			logger("点击等待耗时:",useTime/1000,"秒")
			return true
		end
		mSleep(space)
		useTime = useTime + space
	end
	local errorInfo = "点击等待耗时:"..tostring(useTime/1000).."秒。".."重试比色超时："..JsonUtils.encode(color)
	logger(errorInfo)
	error(errorInfo)
end

function clickCheckRetryPic(x,y,pic,range,num,space)
	local useTime = 0
	if num == nil then
		num = 40
	end
	if space == nil then
		space = 500
	end
	for i=num,1,-1 do
		click(x,y)
		x, y = findPic(pic, range)
		if x ~= -1 then
			logger("点击等待耗时:",useTime/1000,"秒")
			return true
		end
		mSleep(space)
		useTime = useTime + space
	end
	local errorInfo = "点击等待耗时:"..tostring(useTime/1000).."秒。".."重试比色超时："..JsonUtils.encode(color)
	logger(errorInfo)
	error(errorInfo)
end

--初始化参数
function initAction(...)
	local param = {}
	for k,v in pairs({...}) do
		if (v ~= nil) then
			table.insert(param, v)
		end
	end
	return param
end

--判断数组是否包含
function contains(array,val)
	for key,value in ipairs(array) do
		if (val == value) then
			return true
		end
	end
	return false
end

--模拟返回按钮
function back()
	return os.execute("input keyevent KEYCODE_BACK")
end
